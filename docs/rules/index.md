@ngdoc rules
@module rules
@name Available rules
@description

Rules with <span class="fa fa-check"></span> are enabled by
`htmlvalidate:recommended`.<br>
Rules with <span class="fa fa-file-text-o"></span> are enabled by
`htmlvalidate:document`.
