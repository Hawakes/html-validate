@ngdoc content
@module rules
@name no-trailing-whitespace
@category style
@summary Disallow trailing whitespace
@description

# disallows trailing whitespace at the end of lines (`no-trailing-whitespace`)

Lines with trailing whitespace cause unnessecary diff when using version control
and usually serve no special purpose in HTML.

## Rule details
