@ngdoc content
@module rules
@name wcag/h30
@category a17y
@summary WCAG 2.1 H30: Providing link text
@description

# WCAG 2.1 H30: Providing link text (`wcag/h30`)

[WCAG 2.1 technique H30][1] requires each `<a>` anchor link to have a text
describing the purpose of the text. The description may come from plain text or
from an image with alternative text.

The purpose of this rule is to ensure users can distinguish the link from other
links and determine whenever to follow the link or not. Assistive technology may
also present a list of links in which case the description is the only thing the
user will be presented with.

[1]: https://www.w3.org/WAI/WCAG21/Techniques/html/H30

## Rule details

Examples of **incorrect** code for this rule:

<validate name="incorrect" rules="wcag/h30">
	<a><img src="cat.gif"></a>
</validate>

Examples of **correct** code for this rule:

<validate name="correct" rules="wcag/h30">
	<a>lorem ipsum</a>
	<a><img src="cat.gif" alt="cat page"></a>
</validate>
