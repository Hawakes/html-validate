@ngdoc content
@module usage
@name Developers guide
@description

# Developers guide

- [Writing rules](/dev/writing-rules.html)
- [Writing plugins](/dev/writing-plugins.html)
- [Events](/dev/events.html)
- [Transformers](/dev/transformers.html)
