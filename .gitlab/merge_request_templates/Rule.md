## Checklist

- [ ] Changelog updated
- [ ] Documentation updated
- [ ] Rule added to recommended list
- [ ] Change covered by a testcase
- [ ] Smoketest added/updated
- [ ] Commit history cleaned (no WIP, fixups, etc)

/label ~Rule
