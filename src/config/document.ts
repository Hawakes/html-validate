module.exports = {
	rules: {
		"input-missing-label": "error",
		"heading-level": "error",
		"missing-doctype": "error",
	},
};
