/* used when calling require('htmlvalidate'); */

export { default as HtmlValidate } from "./htmlvalidate";
export { AttributeData } from "./parser";
export { Config, ConfigData, ConfigLoader } from "./config";
export { DynamicValue } from "./dom/dynamic-value";
export { Rule } from "./rule";
export { Source } from "./context";
export { TemplateExtractor } from "./transform/template";
